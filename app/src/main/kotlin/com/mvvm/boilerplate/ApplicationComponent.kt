package com.mvvm.boilerplate

import com.mvvm.boilerplate.data.network.NetworkModule
import com.mvvm.boilerplate.data.remote.ApiModule
import com.mvvm.boilerplate.ui.detail.DetailComponent
import com.mvvm.boilerplate.ui.detail.DetailModule
import com.mvvm.boilerplate.ui.list.ListComponent
import com.mvvm.boilerplate.ui.list.ListModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, NetworkModule::class, ApiModule::class])
interface ApplicationComponent {

    // Injectors
    fun injectTo(appMVVM: KotlinMVVMBoilerplateApp)

    // Submodule methods
    // Every screen is its own submodule of the graph and must be added here.
    fun plus(module: ListModule): ListComponent

    fun plus(module: DetailModule): DetailComponent
}