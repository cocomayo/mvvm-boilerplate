package com.mvvm.boilerplate

import android.app.Application
import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import dagger.Module
import dagger.Provides
import timber.log.Timber
import javax.inject.Singleton

@Module
class ApplicationModule(private val appMVVM: KotlinMVVMBoilerplateApp) {

    @Provides @Singleton
    fun provideApplication(): Application = appMVVM

    @Provides @Singleton @ApplicationQualifier
    fun provideContext(): Context = appMVVM.baseContext

    @Provides @Singleton
    fun provideResources(): Resources = appMVVM.resources

    @Provides @Singleton
    fun provideLayoutInflater(@ApplicationQualifier context: Context): LayoutInflater {
        return LayoutInflater.from(context)
    }

    @Provides
    fun provideDebugTree(): Timber.DebugTree = Timber.DebugTree()
}