package com.mvvm.boilerplate

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationQualifier
