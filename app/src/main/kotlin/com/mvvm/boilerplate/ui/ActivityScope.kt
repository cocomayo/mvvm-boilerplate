package com.mvvm.boilerplate.ui

import javax.inject.Scope

@Scope
annotation class ActivityScope