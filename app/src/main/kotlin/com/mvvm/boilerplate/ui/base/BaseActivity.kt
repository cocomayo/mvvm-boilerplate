package com.mvvm.boilerplate.ui.base

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import com.mvvm.boilerplate.ApplicationComponent
import com.mvvm.boilerplate.KotlinMVVMBoilerplateApp

abstract class BaseActivity : AppCompatActivity() {

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies(KotlinMVVMBoilerplateApp.graph)
    }

    abstract fun injectDependencies(graph: ApplicationComponent)
}