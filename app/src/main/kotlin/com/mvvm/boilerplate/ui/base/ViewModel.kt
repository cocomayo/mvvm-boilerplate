package com.mvvm.boilerplate.ui.base

interface ViewModel {

    fun bind()

    fun unbind()

    fun onDestroy()
}