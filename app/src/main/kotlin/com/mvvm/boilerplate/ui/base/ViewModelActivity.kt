package com.mvvm.boilerplate.ui.base

import android.os.Build
import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.databinding.ViewDataBinding
import androidx.loader.app.LoaderManager
import androidx.loader.content.Loader
import javax.inject.Inject
import javax.inject.Provider

abstract class ViewModelActivity<T : ViewModel, B : ViewDataBinding>
    : BaseActivity(),
      LoaderManager.LoaderCallbacks<T> {

    lateinit var viewModel: T
    lateinit var binding: B

    @Inject
    lateinit var viewModelLoaderProvider: Provider<ViewModelLoader<T>>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = getViewBinding()

        initLoader()
    }

    abstract fun getViewBinding(): B

    @CallSuper
    open fun onViewModelProvided(viewModel: T) {
        this.viewModel = viewModel
    }

    @CallSuper
    open fun onViewModelReset() {
        // Hook for subclasses
    }

    @CallSuper
    override fun onStart() {
        super.onStart()
    }

    @CallSuper
    open fun onBind() {
        viewModel.bind()
    }

    // On Nougat and above onStop is no longer lazy!!
    // This makes sure to unbind our viewModel properly
    // See https://www.bignerdranch.com/blog/android-activity-lifecycle-onStop/
    @CallSuper
    override fun onStop() {
        super.onStop()
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            onUnbind()
        }
    }

    @CallSuper
    open fun onUnbind() {
        viewModel.unbind()
    }

    override fun onPause() {
        super.onPause()
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            onUnbind()
        }
    }

    private fun initLoader() {
        LoaderManager.getInstance(this).initLoader(LOADER_ID, null, this)
    }

    override fun onLoadFinished(loader: Loader<T>, data: T) {
        onViewModelProvided(data)
        onBind()
    }

    override fun onCreateLoader(id: Int, bundle: Bundle?): Loader<T> {
        return viewModelLoaderProvider.get()
    }

    override fun onLoaderReset(loader: Loader<T>) {
        onViewModelReset()
    }

    companion object {
        const val LOADER_ID = 1
    }
}