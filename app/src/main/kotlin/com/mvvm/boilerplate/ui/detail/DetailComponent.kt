package com.mvvm.boilerplate.ui.detail

import com.mvvm.boilerplate.ui.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [DetailModule::class])
interface DetailComponent {

    fun injectTo(activity: DetailActivity)
}