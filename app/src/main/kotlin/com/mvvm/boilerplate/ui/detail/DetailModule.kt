package com.mvvm.boilerplate.ui.detail

import androidx.appcompat.app.AppCompatActivity
import com.mvvm.boilerplate.data.remote.model.Repo
import com.mvvm.boilerplate.ui.base.ActivityModule
import dagger.Module
import dagger.Provides

@Module
class DetailModule(activity: AppCompatActivity, val repo: Repo) : ActivityModule(activity) {

    @Provides
    fun provideRepo(): Repo = repo
}