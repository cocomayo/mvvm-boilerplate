package com.mvvm.boilerplate.ui.list

import com.mvvm.boilerplate.ui.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [ListModule::class])
interface ListComponent {

    fun injectTo(activity: ListActivity)
}