package com.mvvm.boilerplate.ui.list

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mvvm.boilerplate.R
import com.mvvm.boilerplate.data.remote.model.Repo
import com.mvvm.boilerplate.databinding.ItemRepoBinding
import com.mvvm.boilerplate.ui.ActivityScope
import javax.inject.Inject

@ActivityScope
class RepoAdapter @Inject constructor() : RecyclerView.Adapter<RepoAdapter.RepoViewHolder>() {

    private var repos: List<Repo> = emptyList()
    private var itemClick: ((Repo) -> Unit)? = null

    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) {
        val binding = holder.binding
        val repo = repos[position]
        var viewModel = binding.viewModel

        // Unbind old  iewModel if we have one
        viewModel?.unbind()

        // Create new ViewModel, set it, and bind it
        viewModel = RepoViewModel(repo)
        binding.viewModel = viewModel
        viewModel.bind()

        holder.setClickListener(itemClick)
    }

    override fun getItemCount(): Int = repos.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        val binding = DataBindingUtil.inflate<ItemRepoBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_repo,
            parent,
            false
        )

        return RepoViewHolder(binding)
    }

    fun updateRepos(repos: List<Repo>) {
        val diff = RepoDiffCallback(this.repos, repos)
        val result = DiffUtil.calculateDiff(diff)

        this.repos = repos
        result.dispatchUpdatesTo(this)
    }

    fun setClickListener(itemClick: ((Repo) -> Unit)?) {
        this.itemClick = itemClick
    }

    class RepoViewHolder(val binding: ItemRepoBinding) : RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("CheckResult")
        fun setClickListener(callback: ((Repo) -> Unit)?) {
            binding.viewModel?.clicks()?.subscribe {
                callback?.invoke(binding.viewModel!!.repo)
            }
        }

    }
}