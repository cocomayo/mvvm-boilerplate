package com.mvvm.boilerplate.ui.list

import com.mvvm.boilerplate.data.remote.model.Owner
import com.mvvm.boilerplate.data.remote.model.Repo
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class RepoViewModelTest {
    private lateinit var owner: Owner
    private lateinit var repo: Repo
    private lateinit var viewModel: RepoViewModel

    @Before
    fun setUp() {
        owner = Owner("Author",
            "someURL")

        repo = Repo("Name",
            "Author/Name",
            owner,
            "Some random repo",
            50,
            100)

        viewModel = RepoViewModel(repo)
        viewModel.bind()
    }

    @Test
    fun getName_returnsCorrectName() {
        Assert.assertEquals(viewModel.getName(), repo.fullName)
    }

    @Test
    fun getDescription_returnsCorrectDescription() {
        Assert.assertEquals(viewModel.getDescription(), repo.description)
    }

    @Test
    fun clicks_returnsNoClicks() {
        viewModel.clicks().test().assertNoValues()
    }

    @Test
    fun clicks_clicksOnce() {
        val observer = viewModel.clicks().test()

        viewModel.onClick()
        observer.assertValueCount(1)
    }
}